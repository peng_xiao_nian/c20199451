程序代码：
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
 
int menu();
int help();
int operation_1();
int operation_2();
int operation_3();
int error();
int topic1(int n); 
int topic2(int n);
int topic3(int n);

int main()
{
	printf("========== 口算生成器 ==========\n欢迎使用口算生成器；)\n\n "); 
	
	help();
	int opt=-1;
    while(opt!=5)
	{
		printf("操作列表:\n1)一年级    2)二年级    3)三年级\n4)帮助      5)退出程序\n请输入操作>");
		scanf("%d",&opt);
		printf("<执行操作：）\n\n");
		
		
		switch(opt)
		{
			case 1:operation_1();break;
			case 2:operation_2();break;
			case 3:operation_3();break;
			case 4:help(); break;
			case 5:printf("程序结束, 欢迎下次使用任意键结束……");break;	
            default :error();break;	
		} 
	}
	return 0; 
}
help() 
{
	printf("帮助信息\n您需要输入命令代号来进行操作, 且\n一年级题目为不超过十位的加减法;\n二年级题目为不超过百位的乘除法;\n");
	printf("三年级题目为不超过百位的加减乘除混合题目.\n\n");
}
operation_1()
{
	printf("现在是一年级题目：\n请输入生成数字〉");
	int n;
	scanf("%d",&n);
	printf("<执行操作：）\n\n");
        topic1(n); 
	printf("\n\n");
}
operation_2()
{
	printf("现在是二年级题目：\n请输入生成数字〉");
	int n=0;
	scanf("%d",&n);
	printf("<执行操作：）\n\n");
	topic2(n);
	printf("\n\n");
}
operation_3()
{
	printf("现在是三年级题目：\n请输入生成数字>:");
	int n;
        scanf("%d",&n);
	printf("<执行操作：）\n\n");
	topic3(n);
	printf("\n\n");	
} 
error()
{
	printf("Error!!!\n错误操作指令, 请重新输入\n\n");
}

int topic1(int n)
{
	int i;
	char a[2]={'+','-'};
	time_t t;
	srand((unsigned) time(&t));
 
   for( i = 0 ; i <n ; i++ )
	{ 
	  printf("%2d %c %2d = __\n",rand()%10,a[rand()%2],rand()%10);
    }
}
int topic2(int n)
{
	int i;
	char a[2]={'*','/'};
	time_t t;
	srand((unsigned) time(&t));
	for(i=0;i<n;i++)
	{
	    printf("%2d %c %2d = __\n",rand()%100,a[rand()%2],rand()%100+1);
    }
}
int topic3(int n)
{
	int i;
	char a[4]={'+','-','/','*'};
	time_t t;
	srand((unsigned) time(&t));
	for(i=0;i<n;i++)
	{
	    printf("%2d %c %2d %c %2d= __\n",rand()%100,a[rand()%4],rand()%100+1,a[rand()%4],rand()%100+1);
    }
}